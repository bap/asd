SOURCES         = src/asd.cc                    \
                  src/gfx/draw.cc               \
                  src/gfx/chunks_drawer.cc      \
                  src/gfx/color.cc              \
                  src/gfx/view.cc               \
                  src/cubefield/cubefield.cc    \
                  src/cubefield/cube.cc         \
                  src/cubefield/chunk.cc        \
                  src/ship.cc                   \
                  src/world.cc                  \
                  src/camera.cc                 \
                  src/gfx/textureManager.cc     \
                  src/gfx/fontManager.cc        \
                  src/event/events/event.cc     \
                  src/key/keymap.cc             \
                  src/event/event_manager.cc    \
                  src/debris.cc                 \
                  src/world_object.cc

_OBJS           = $(SOURCES:src/%.cc=$(OBJS_DIR)/%.o)
OBJS_DIR        = .objs

LDFLAGS         += -L. -Lsrc
LDLIBS		+= -lSDL -lSDL_ttf
CXXFLAGS        += -O2 -Wall -Wextra -pedantic -Weffc++ -std=c++11 -Isrc -I.

CXX             = clang++

BIN             = asd

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S), Linux)
  SOURCES	+= src/soundManager.cc
  LDLIBS	+= -lm -lGL -lGLU -lfmodex
endif

ifeq ($(UNAME_S), Darwin)
  LDLIBS	+= -lSDLmain -framework Cocoa -framework OpenGL -lc++
  CXXFLAGS	+= -stdlib=libc++
endif

# Test the existence of .Makefile.dep
$(shell                                                                 \
if ! [ -f .Makefile.dep ]; then                                         \
  $(RM) .Makefile.dep &&                                                \
  for f in $(SOURCES); do                                               \
    obj=$(OBJS_DIR)/$${f#src/};                                         \
     $(CXX) $(CXXFLAGS) -MT$${obj%.*}.o -MM $$f>> .Makefile.dep;        \
  done;                                                                 \
fi)

_CLEAN        = echo "test -z \"$$f\" || ! test -f \"$$f\" || $(RM) $$f";     \
                test -z "$$f" || ! test -f "$$f" || $(RM) $$f
_CLEAN_R      = echo "test -z \"$$f\" || ! test -r \"$$f\" || $(RM) -r $$f";  \
                test -z "$$f" || ! test -r "$$f" || $(RM) -r $$f

all: $(BIN)

$(BIN): $(_OBJS)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@

.objs/%.o: src/%.cc src/%.h
	@test -d `dirname $@` || mkdir -p `dirname $@`
	$(CXX) -c $(CXXFLAGS) -o $@     $<

include .Makefile.dep

LINTFLAGS=-readability/streams,-whitespace/braces,-legal,-whitespace/newline,-runtime/threadsafe_fn,-build/header_guard

check:
	@for f in $(shell find src -type f); do \
	  python cpplint.py --filter=$(LINTFLAGS) "$$f" 2>&1; \
	done

clean:
	@for f in $(_OBJS) $(BIN); do   \
	  $(_CLEAN);                    \
	done
	@f=$(OBJS_DIR);                 \
	$(_CLEAN_R)


distclean: clean
	@for f in .Makefile.dep; do         \
	  $(_CLEAN);                        \
	done
