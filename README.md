ASD
===

Some kind of game.

### Authors

Marc Ferry

Jeremy Tran

Benoit Joseph

Baptiste Marchand

### Dependencies

[SDL](http://www.libsdl.org/)
[SDL_ttf](http://www.libsdl.org/projects/SDL_ttf/)
[GLM](http://glm.g-truc.net/0.9.4/index.html)
