#include "./ship.h"

#ifndef __APPLE__
# include <GL/gl.h>
#else
# include <OpenGL/gl.h>
#endif
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "gfx/draw.h"
#include "./asd.h"
#include "cubefield/cubefield.h"
#include "./world.h"

Ship::Ship()
  : WorldObject(),
    god_mode(false),
    score(0),
    highscore(0),
    old_score(0),
    mesh_(new Cube(glm::vec3(-CHK_SIZE / 4.0,
                             CUBE_SIZE / 2.0,
                             0.0))),
    color_(gfx::Color(1, 1, 1)),
    broken_(false),
    debris_(nullptr)
{
  position = mesh_->center;
  acceleration = 0.05;
  friction = 0.0;
  mesh_->set_color(1, 1, 1);
}

Ship::~Ship()
{
}

void Ship::setColor(int r, int g, int b)
{
  color_.r = r;
  color_.g = g;
  color_.b = b;
  mesh_->set_color(r , g, b);
}

void Ship::addSpeed(float x, float y, float z)
{
  speed.x += x;
  if (speed.x > MAX_SHIP_SPEED)
    speed.x = MAX_SHIP_SPEED;
  if (speed.x < -MAX_SHIP_SPEED)
    speed.x = -MAX_SHIP_SPEED;

  speed.y += y;
  if (speed.y > MAX_SHIP_SPEED)
    speed.y = MAX_SHIP_SPEED;
  if (speed.y < -MAX_SHIP_SPEED)
    speed.y = -MAX_SHIP_SPEED;

speed.z += z;
  if (speed.z > MAX_SHIP_SPEED)
    speed.z = MAX_SHIP_SPEED;
  if (speed.z < -MAX_SHIP_SPEED)
    speed.z = -MAX_SHIP_SPEED;
}

void Ship::repair()
{
  broken_ = false;
  debris_.reset(nullptr);
  mesh_->set_center(-CHK_SIZE / 4.0,
                    CUBE_SIZE / 2.0,
                    0.0);
  speed = glm::vec3(0.0,
                    0.0,
                    0.0);
  position = mesh_->center;
  old_score += score;
}

void Ship::update(unsigned int etime, World *world)
{
  if (!god_mode && !broken_ && world->cubefield()->collision(position))
  {
#ifndef __APPLE__
    soundManager.play("crash");
#endif
    debris_.reset(new Debris(mesh_->center));
    broken_ = true;
  }


  // update color
  if (color_.g < 1.0)  // if the ship is red
  {
    color_.g += 0.002*etime;
    color_.b += 0.002*etime;
    mesh_->set_color(color_);
  }

  // Update debris
  if (debris_)
    debris_->update(etime);

  if (broken_)
    return;

  // update score
  score = world->time - old_score;

  if ((score > highscore) && (speed.x > 0.001))
    highscore = score;

  // Apply current speed
  mesh_->set_center(mesh_->center.x + speed.x * etime/2.0,
                    mesh_->center.y + speed.y * etime/2.0,
                    mesh_->center.z + speed.z * etime/2.0);
  position = mesh_->center;

  // Apply friction
  if (fabs(speed.x) > 0.00001)
    speed.x += (speed.x > 0
                 ? -friction * etime/1000.0
                 :  friction * etime/1000.0);

  if (fabs(speed.y) > 0.00001)
    speed.y += (speed.y > 0
                 ? -friction * etime/1000.0
                 :  friction * etime/1000.0);

  if (fabs(speed.z) > 0.00001)
    speed.z += (speed.z > 0
                 ? -friction * etime/1000.0
                 :  friction * etime/1000.0);
}

void Ship::draw() const
{
  if (!broken_)
    gfx::draw_cube(*mesh_);
  else if (debris_)
    debris_->draw();
}
