#ifndef WORLD_OBJECT_H_
# define WORLD_OBJECT_H_

#include <glm/glm.hpp>

class WorldObject
{
 public:
  WorldObject();
  virtual ~WorldObject();

  glm::vec3 position;
  glm::vec3 scale;
  glm::vec3 rotation;

  glm::vec3 speed;
  float acceleration;
  float friction;
};

#endif /* !WORLD_OBJECT_H_ */
