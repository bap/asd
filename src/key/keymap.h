#ifndef KEYMAP_H_
# define KEYMAP_H_

# include <map>
# include <functional>

class World;
class Camera;

namespace key
{
  enum keyState
  {
    PRESSED,
    RELEASED
  };

  struct handlerParams
  {
      World      *world;
      Camera     *cam;
      keyState   state;
  };

  typedef std::function<void(handlerParams&)> keyHandler;

  struct KeyData
  {
    KeyData(keyState lastState = RELEASED, keyHandler handler = nullptr)
      : lastState(lastState),
        handler(handler)
    {}
    keyState   lastState;
    keyHandler handler;
  };

  class KeyMap
  {
    public:
      typedef int key_type;
      typedef std::map<key_type, KeyData> t_key_map;

      KeyMap();
      explicit KeyMap(const t_key_map& map);
      ~KeyMap();

      void map_key(key_type key, const keyHandler handler);
      void handle_key(key_type key, World *world, Camera *cam)
      {
          handlerParams params{world, cam, key_map_[key].lastState};

          key_map_[key].handler(params);
          key_map_[key].lastState = PRESSED;
      }
      void release_key(key_type key) { key_map_[key].lastState = RELEASED; }

      // Getter
      const t_key_map& get_key_map() const { return key_map_; }

    private:
      t_key_map key_map_;
  };
}  // namespace key

#endif /* !KEYMAP_H_ */
