#include "./keymap.h"

namespace key
{
  KeyMap::KeyMap()
    : key_map_(t_key_map())
  {
  }

  KeyMap::KeyMap(const t_key_map& map)
    : key_map_(map)
  {
  }

  KeyMap::~KeyMap()
  {
  }

  void
  KeyMap::map_key(key_type         key,
                  const keyHandler handler)
  {
    KeyData keydata(RELEASED, handler);
    key_map_[key] = keydata;
  }
}  // namespace key
