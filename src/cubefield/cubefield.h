#ifndef CUBEFIELD_H_
# define CUBEFIELD_H_

#include <SDL/SDL.h>
#ifndef __APPLE__
#include <GL/gl.h>
#else
#include <OpenGL/gl.h>
#endif

#include <vector>
#include <memory>

#include "./chunk.h"
#include "../gfx/chunks_drawer.h"

class World;

class Cubefield
{
 public:
  Cubefield();
  virtual ~Cubefield();
  void draw() const;
  void update(unsigned int etime, World *world);
  bool collision(const glm::vec3 &ship_pos);

 private:
  Cubefield(const Cubefield& cubefield) = delete;
  Cubefield& operator=(const Cubefield& cubefield) = delete;

  std::vector<std::unique_ptr<Chunk>> chunks_;
  gfx::ChunksDrawer drawer_;
};

#endif  // CUBEFIELD_H_
