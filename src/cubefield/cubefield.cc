#include <cstdlib>
#include <cmath>
#include <iostream>
#include "../gfx/draw.h"
#include "./cubefield.h"
#include "./world.h"

Cubefield::Cubefield()
  : chunks_(),
    drawer_()
{
}

Cubefield::~Cubefield()
{
}

void Cubefield::draw() const
{
  drawer_.draw();
}

bool Cubefield::collision(const glm::vec3 &ship_pos)
{
  float x = floor(ship_pos.x / CHK_SIZE);
  x *= CHK_SIZE;
  x += CHK_SIZE/2.0;
  float z = floor(ship_pos.z / CHK_SIZE);
  z *= CHK_SIZE;
  z += CHK_SIZE/2.0;

  for (auto& chunk : chunks_)
  {
    if (chunk->position.x == x && chunk->position.z == z)
      return chunk->collision(ship_pos);
  }

  return false;
}

void Cubefield::update(unsigned int /*etime*/, World *world)
{
  glm::vec3 ship_pos = world->ship()->position;
  // Hide chunk if it's too far away
  for (auto& chunk : chunks_)
  {
    float m = 0;  // manhattan distance
    m += fabs(chunk->position.x - ship_pos.x);
    m += fabs(chunk->position.z - ship_pos.z);
    chunk->setVisible(m <= 150);
  }

  // Create Chunk if needed
  // (x,z) is the position of the chunk we are currently on
  float x = floor(ship_pos.x / CHK_SIZE);
  x *= CHK_SIZE;
  x += CHK_SIZE/2.0;
  float z = floor(ship_pos.z / CHK_SIZE);
  z *= CHK_SIZE;
  z += CHK_SIZE/2.0;

  bool found = false;
  const int r = 2;
  for (float cx = x - CHK_SIZE * r; cx <= x + CHK_SIZE*r; cx += CHK_SIZE)
    for (float cz = z - CHK_SIZE * r; cz <= z + CHK_SIZE * r; cz += CHK_SIZE)
    {
      found = false;
      for (auto& chunk : chunks_)
        if (fabs(chunk->position.x - cx) < 1.0 &&
            fabs(chunk->position.z - cz) < 1.0)
        {
          found = true;
          break;
        }

      if (cx >= CHK_SIZE / 2.0)
      {
        if (!found)
        {
          Chunk *c = new Chunk(glm::vec3(cx, 0, cz));
          chunks_.push_back(std::unique_ptr<Chunk>(c));
          drawer_.addChunk(*(chunks_.back().get()));
        }
      }
    }
}

