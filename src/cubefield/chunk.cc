#include "./chunk.h"

#include <cstdlib>
#include "../gfx/color.h"

inline void addRandomDisplacement(glm::vec3 *v, int max)
{
  v->x += rand() % max;
  v->z += rand() % max;
}

Chunk::Chunk(const glm::vec3 &center)
  : WorldObject(),
    cubes_(std::vector<Cube>()),
    visible_(true)
{
  position = center;
  float spacing = CHK_SIZE / NB_CUBES;
  gfx::Color c(0.5, 0.5, 0.5);
  for (int i = - NB_CUBES/2; i < NB_CUBES/2; i++)
    for (int j = - NB_CUBES/2; j < NB_CUBES/2; j++)
    {
      glm::vec3 p(center.x + i * spacing,
                  center.y + CUBE_SIZE/2.0,
                  center.z + j * spacing);
      addRandomDisplacement(&p, spacing);
      cubes_.push_back(Cube(p));
      c = gfx::Color(c.r + (rand()%100 - 50)/800.0,
                     c.g + (rand()%100 - 50)/800.0,
                     c.b + (rand()%100 - 50)/800.0);
      cubes_.back().set_color(c);
    }
}

Chunk::~Chunk()
{
}

bool Chunk::collision(const glm::vec3 &ship_pos)
{
  for (auto it = cubes_.begin(); it != cubes_.end(); it++)
  {
    if (it->collision(ship_pos))
      return true;
  }
  return false;
}
