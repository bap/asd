#include "./cube.h"

#ifndef __APPLE__
# include <GL/gl.h>
#else
# include <OpenGL/gl.h>
#endif
#include <cstdlib>
#include <cmath>
#include "../gfx/draw.h"

// Create a cube with random angle and color
Cube::Cube(const glm::vec3 &center)
  : speed(glm::vec3(0.0f)),
    center(center),
    size_(CUBE_SIZE),
    color_(gfx::Color())
{
}

void Cube::set_color(float r, float g, float b)
{
  color_ = gfx::Color(r, g, b);
}

void Cube::set_center(float x, float y, float z)
{
  center.x = x;
  center.y = y;
  center.z = z;
}

bool Cube::collision(const glm::vec3 &ship_pos)
{
  float dx = fabs(ship_pos.x - center.x);
  float dz = fabs(ship_pos.z - center.z);

  return dx < CUBE_SIZE && dz < CUBE_SIZE;
}
