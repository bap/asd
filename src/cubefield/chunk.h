#ifndef CHUNK_H_
# define CHUNK_H_

#ifndef __APPLE__
# include <GL/gl.h>
#else
# include <OpenGL/gl.h>
#endif
# include <vector>

# include "../world_object.h"
# include "./cube.h"

# define NB_CUBES 6
// size of a chunk
#define CHK_SIZE 50.0

class Chunk : public WorldObject
{
 public:
  explicit Chunk(const glm::vec3 &center);
  virtual ~Chunk();

  bool collision(const glm::vec3 &ship_pos);

  const std::vector<Cube>& cubes() const { return cubes_; }
  void setVisible(bool visible) { visible_= visible; }
  bool visible() const { return visible_; }

 private:
  Chunk(const Chunk& chunk) = delete;
  Chunk& operator=(const Chunk& chunk) = delete;

  std::vector<Cube> cubes_;
  bool visible_;
};

#endif /* !CHUNK_H_ */
