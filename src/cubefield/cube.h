#ifndef CUBE_H_
# define CUBE_H_

#include <glm/glm.hpp>

# include "../gfx/color.h"

# define CUBE_SIZE 1.0

class Cube
{
 public:
  explicit Cube(const glm::vec3 &center);
  float size()   const{ return size_; }
  const gfx::Color& color()  const { return color_;}
  void  set_color(gfx::Color c) {color_ = c;}
  void  set_color(float r, float g, float b);
  void  set_center(float x, float y, float z);
  bool collision(const glm::vec3 &ship_pos);

  glm::vec3 speed;
  glm::vec3 center;

 private:
  float size_;
  gfx::Color color_;
};

#endif  // CUBE_H_
