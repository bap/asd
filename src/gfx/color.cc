#include "./color.h"

#ifndef __APPLE__
# include <GL/gl.h>
#else
# include <OpenGL/gl.h>
#endif
#include <cstdlib>

namespace gfx
{
#define clamp(x) (x < 0.0 ? 0.0 : (x > 1.0 ? 1.0 : x))
  Color::Color(float r, float g, float b)
    : r(clamp(r)),
      g(clamp(g)),
      b(clamp(b))
  {
  }

  Color::Color()
    : r(rand() % 255 / 255.0),
      g(rand() % 255 / 255.0),
      b(rand() % 255 / 255.0)
  {
  }

  void Color::use() const
  {
    GLfloat c[4] = {r, g, b, 1.0};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, c);
  }

  void Color::use(float r, float g, float b)
  {
    GLfloat c[4] = {r, g, b, 1.0};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, c);
  }
}  // namespace gfx
