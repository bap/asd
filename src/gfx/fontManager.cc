#include "./fontManager.h"
#ifndef __APPLE__
# include <GL/glu.h>
#else
# include <OpenGL/glu.h>
#endif
#include <iostream>

namespace gfx
{
  FontManager::FontManager()
    : font(nullptr)
  {
    if (!(font = TTF_OpenFont("media/font/DejaVuSans.ttf", 100)))
      std::cout << "Error loading font: " << TTF_GetError() << std::endl;
    else
      std::cout << "Font loaded" << std::endl;
  }
}  // namespace gfx
