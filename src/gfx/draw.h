/*
 * draw.h
 *
 * Functions to draw common objects.
 * All the functions assume the 'up' vector is (0, 1, 0)
 */

#ifndef DRAW_H
# define DRAW_H

#include <glm/glm.hpp>
#include <functional>
#include "cubefield/cube.h"

namespace gfx
{
  void draw(std::function<void()>&);

  /*
   * Draw a cube centered at the origin.
   * Size: 2x2x2
   */
  void draw_cube(const Cube&, float scale = 1.0);

  /*
   * Debug function showing the axis
   * X: red
   * Y: green
   * Z: blue
   */
  void draw_axes();

  /*
   * Draw a plane
   */
  void draw_floor(const glm::vec3 size, const glm::vec3 center);

  void draw_score(unsigned int score, unsigned int highscore);
}  // namespace gfx

#endif
