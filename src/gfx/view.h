#ifndef VIEW_H_
# define VIEW_H_

#include "../camera.h"

namespace gfx
{
  void update_view(const Camera&);
  void enable2D();
  void disable2D();
}


#endif  // VIEW_H_
