#ifndef FONT_MANAGER_H
#define FONT_MANAGER_H

#ifndef __APPLE__
# include <GL/gl.h>
#else
# include <OpenGL/gl.h>
#endif

#include <SDL/SDL_ttf.h>

namespace gfx
{
  class FontManager
  {
  public:
    static FontManager& getInstance()
    {
      static FontManager instance;
      return instance;
    }

    TTF_Font *font;

  private:
    FontManager();
    explicit FontManager(FontManager const&);
    void operator=(FontManager const&);
  };
}  // namespace gfx

#endif
