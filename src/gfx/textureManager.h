#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#ifndef __APPLE__
# include <GL/gl.h>
#else
# include <OpenGL/gl.h>
#endif

#include <map>
#include <string>

namespace gfx
{
  class TextureManager
  {
    public:
      TextureManager();
      ~TextureManager();
      void add(const std::string &name,
               const void *data,
               GLsizei size);
      void use(const std::string &name);

    private:
      std::map<std::string, GLuint> _map;
  };
}  // namespace gfx

#endif
