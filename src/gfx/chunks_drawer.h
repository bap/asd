#ifndef CHUNKS_DRAWER_H_
#define CHUNKS_DRAWER_H_

# ifndef __APPLE__
#  include <GL/gl.h>
# else
#  include <OpenGL/gl.h>
# endif
# include <map>

# include "../cubefield/chunk.h"

namespace gfx
{
  class ChunksDrawer
  {
    public:
      ChunksDrawer();
      ~ChunksDrawer();

      void addChunk(const Chunk&);
      void removeChunk(const Chunk& chunk);
      void draw() const;

    private:
      std::map<const Chunk* const, GLuint> displayMap;
  };
}  // namespace gfx

#endif  // CHUNKS_DRAWER_H_
