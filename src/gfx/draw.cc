#include "./draw.h"

#ifndef __APPLE__
# include <GL/gl.h>
# include <GL/glu.h>
#else
# include <OpenGL/gl.h>
#endif

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include <iostream>
#include <string>

#include "./textureManager.h"
#include "./fontManager.h"
#include "./view.h"

extern gfx::TextureManager textureManager;

namespace
{
  inline void draw_cube_faces()
  {
    const int tex_repeat = 4;
    glBegin(GL_QUADS);

    // Bottom
    glNormal3f(0, -1, 0);
    glTexCoord2f(0, 0);
    glVertex3f(-0.5, -0.5, -0.5);
    glTexCoord2f(0, tex_repeat);
    glVertex3f(-0.5, -0.5, 0.5);
    glTexCoord2f(tex_repeat, tex_repeat);
    glVertex3f(0.5, -0.5, 0.5);
    glTexCoord2f(tex_repeat, 0);
    glVertex3f(0.5, -0.5, -0.5);

    // Top
    glNormal3f(0, 1, 0);
    glTexCoord2f(0, 0);
    glVertex3f(-0.5, 0.5, -0.5);
    glTexCoord2f(tex_repeat, 0);
    glVertex3f(0.5, 0.5, -0.5);
    glTexCoord2f(tex_repeat, tex_repeat);
    glVertex3f(0.5, 0.5, 0.5);
    glTexCoord2f(0, tex_repeat);
    glVertex3f(-0.5, 0.5, 0.5);

    // back
    glNormal3f(0, 0, -1);
    glTexCoord2f(0, 0);
    glVertex3f(-0.5, -0.5, -0.5);
    glTexCoord2f(tex_repeat, 0);
    glVertex3f(0.5, -0.5, -0.5);
    glTexCoord2f(tex_repeat, tex_repeat);
    glVertex3f(0.5, 0.5, -0.5);
    glTexCoord2f(0, tex_repeat);
    glVertex3f(-0.5, 0.5, -0.5);

    // front
    glNormal3f(0, 0, 1);
    glTexCoord2f(0, 0);
    glVertex3f(-0.5, -0.5, 0.5);
    glTexCoord2f(0, tex_repeat);
    glVertex3f(-0.5, 0.5, 0.5);
    glTexCoord2f(tex_repeat, tex_repeat);
    glVertex3f(0.5, 0.5, 0.5);
    glTexCoord2f(tex_repeat, 0);
    glVertex3f(0.5, -0.5, 0.5);

    // left
    glNormal3f(-1, 0, 0);
    glTexCoord2f(0, 0);
    glVertex3f(-0.5, -0.5, -0.5);
    glTexCoord2f(0, tex_repeat);
    glVertex3f(-0.5, 0.5, -0.5);
    glTexCoord2f(tex_repeat, tex_repeat);
    glVertex3f(-0.5, 0.5, 0.5);
    glTexCoord2f(tex_repeat, 0);
    glVertex3f(-0.5, -0.5, 0.5);

    // right
    glNormal3f(1, 0, 0);
    glTexCoord2f(0, 0);
    glVertex3f(0.5, -0.5, -0.5);
    glTexCoord2f(tex_repeat, 0);
    glVertex3f(0.5, -0.5, 0.5);
    glTexCoord2f(tex_repeat, tex_repeat);
    glVertex3f(0.5, 0.5, 0.5);
    glTexCoord2f(0, tex_repeat);
    glVertex3f(0.5, 0.5, -0.5);

    glEnd();
  }
}  // namespace

namespace gfx
{
  void draw(std::function<void()>& drawCallback)
  {
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    drawCallback();
    SDL_GL_SwapBuffers();
  }

  void draw_cube(const Cube& cube, float scale)
  {
    glPushMatrix();
    cube.color().use();
    glTranslatef(cube.center.x, cube.center.y, cube.center.z);
    glScalef(scale, scale, scale);
    draw_cube_faces();
    glPopMatrix();
  }


  void draw_axes()
  {
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);

    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(9000, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 9000, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 9000);

    glEnd();
    glEnable(GL_LIGHTING);
  }

  void draw_floor(const glm::vec3 size, const glm::vec3 center)
  {
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_FOG);
    textureManager.use("floor_grid");

    const glm::vec3 tex_repeat = size;
    glNormal3f(0, 1, 0);
    glBegin(GL_QUADS);
    {
      glTexCoord2f(0, 0);
      glVertex3f(center.x-size.x, center.y, center.z-size.z);

      glTexCoord2f(tex_repeat.x, 0);
      glVertex3f(center.x+size.x, center.y, center.z-size.z);

      glTexCoord2f(tex_repeat.x, tex_repeat.z);
      glVertex3f(center.x+size.x, center.y, center.z+size.z);

      glTexCoord2f(0, tex_repeat.z);
      glVertex3f(center.x-size.x, center.y, center.z+size.z);
    }
    glEnd();
    glEnable(GL_FOG);
    glDisable(GL_TEXTURE_2D);
  }

  int round(double x)
  {
    return static_cast<int>(x + 0.5);
  }

  int nextpoweroftwo(int x)
  {
    double logbase2 = log(x) / log(2);
    return round(pow(2, ceil(logbase2)));
  }

  void renderText(const char *text,
                  TTF_Font *font,
                  SDL_Color color,
                  SDL_Rect *location)
  {
    SDL_Surface *initial;
    SDL_Surface *intermediary;
    int w, h;
    unsigned int texture;

    glBlendFunc(GL_ONE, GL_ONE);
    glEnable(GL_BLEND);

    /* Use SDL_TTF to render our text */
    initial = TTF_RenderText_Solid(font, text, color);

    /* Convert the rendered text to a known format */
    w = nextpoweroftwo(initial->w);
    h = nextpoweroftwo(initial->h);

    intermediary = SDL_CreateRGBSurface(0, w, h, 32,
                                        0x00ff0000, 0x0000ff00,
                                        0x000000ff, 0xff000000);

    SDL_BlitSurface(initial, 0, intermediary, 0);

    /* Tell GL about our new texture */
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA,
                 GL_UNSIGNED_BYTE, intermediary->pixels);

    /* GL_NEAREST looks horrible, if scaled... */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    /* prepare to render our texture */
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glColor3f(0, 0, 0);

    /* Draw a quad at location */
    glBegin(GL_QUADS);
    /* Recall that the origin is in the lower-left corner
       That is why the TexCoords specify different corners
       than the Vertex coors seem to. */
    glTexCoord2f(0.0f, 1.0f);
    glVertex2f(location->x    , location->y);
    glTexCoord2f(1.0f, 1.0f);
    glVertex2f(location->x + w, location->y);
    glTexCoord2f(1.0f, 0.0f);
    glVertex2f(location->x + w, location->y + h);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2f(location->x    , location->y + h);
    glEnd();

    /* Bad things happen if we delete the texture before it finishes */
    glFinish();

    glDisable(GL_BLEND);

    /* return the deltas in the unused w,h part of the rect */
    location->w = initial->w;
    location->h = initial->h;

    /* Clean up */
    SDL_FreeSurface(initial);
    SDL_FreeSurface(intermediary);
    glDeleteTextures(1, &texture);
  }

  inline unsigned int min(unsigned int a, unsigned int b)
  {
    return a < b ? a : b;
  }

  void draw_score(unsigned int score, unsigned int highscore)
  {
    const SDL_VideoInfo *info = SDL_GetVideoInfo();
    SDL_Color color;
    SDL_Rect position;
    enable2D();

    color.r = min(255, (score/20)*255/20);
    color.g = 255-min(255, (score/20)*255/20);
    color.b = 0.1*255;

    glColor3f(0, 0, 0);
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(0, 0);
    glVertex2f(0, 110);
    glVertex2f(200, 140);
    glVertex2f(300, 0);
    glEnd();

    glColor3f(color.r/255.0, color.g/255.0, color.b/255.0);
    glBegin(GL_LINE_STRIP);
    glVertex2f(0, 110);
    glVertex2f(200, 140);
    glVertex2f(300, 0);
    glEnd();

    position.x = 10;
    position.y = -10;
    TTF_Font *font = FontManager::getInstance().font;
    renderText(std::to_string(score).c_str(), font, color, &position);

    color.r = min(255, (score/20)*255/20);
    color.b = 255-min(255, (score/20)*255/20);
    color.g = 0.1*255;

    glColor3f(0, 0, 0);
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(info->current_w, 0);
    glVertex2f(info->current_w, 110);
    glVertex2f(info->current_w - 200, 140);
    glVertex2f(info->current_w - 300, 0);
    glEnd();

    glColor3f(color.r/255.0, color.g/255.0, color.b/255.0);
    glBegin(GL_LINE_STRIP);
    glVertex2f(info->current_w, 110);
    glVertex2f(info->current_w - 200, 140);
    glVertex2f(info->current_w - 300, 0);
    glEnd();

    position.x = info->current_w - 200;
    position.y = -10;
    renderText(std::to_string(highscore).c_str(), font, color, &position);

    disable2D();
  }

}  // namespace gfx
