#ifndef COLOR_H_
# define COLOR_H_

namespace gfx
{
  class Color
  {
    public:
      // Create a random color
      Color();
      Color(float r, float g, float b);
      void use() const;  // Use this color (glColor3f)
      static void use(float r, float g, float b);

      float r, g, b;
  };
}  // namespace gfx

#endif /* !COLOR_H_ */
