#include "./chunks_drawer.h"
#include <utility>
#include "./draw.h"

namespace gfx
{
  ChunksDrawer::ChunksDrawer()
    : displayMap()
  {
  }

  ChunksDrawer::~ChunksDrawer()
  {
    for (auto& kv : displayMap)
      glDeleteLists(kv.second, 1);
  }

  void ChunksDrawer::addChunk(const Chunk& chunk)
  {
    GLuint dl = glGenLists(1);

    displayMap.insert(std::pair<const Chunk* const, GLuint>(&chunk, dl));

    // We are not actually drawing the cubes, we are recording the draw calls
    glNewList(dl, GL_COMPILE);
    for (auto& cube : chunk.cubes())
    {
      draw_cube(cube);
    }
    glEndList();
  }

  void ChunksDrawer::removeChunk(const Chunk& chunk)
  {
    auto it = displayMap.find(&chunk);

    if (it != displayMap.end())
    {
      glDeleteLists(it->second, 1);
      displayMap.erase(it);
    }
  }

  void ChunksDrawer::draw() const
  {
    for (auto& kv : displayMap)
    {
      if (kv.first->visible())
      {
        glCallList(kv.second);
        draw_floor(glm::vec3(CHK_SIZE), kv.first->position);
      }
    }
  }
}  // namespace gfx
