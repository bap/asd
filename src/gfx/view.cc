#include "./view.h"

#ifndef __APPLE__
# include <GL/glu.h>
#else
# include <OpenGL/glu.h>
#endif

namespace gfx
{
  void update_view(const Camera& camera)
  {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glRotatef(camera.roll(), 0, 0, 1);
    gluLookAt(camera.x(), camera.y(), camera.z(),
              camera.target().x, camera.target().y, camera.target().z,
              0, 1, 0);
  }

  void enable2D()
  {
    int vPort[4];

    glGetIntegerv(GL_VIEWPORT, vPort);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glOrtho(0, vPort[2], 0, vPort[3], -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_CULL_FACE);
  }

  void disable2D()
  {
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
  }
}  // namespace gfx
