#ifndef DEBRIS_H_
# define DEBRIS_H_

#include <vector>

# include "cubefield/cube.h"
# include "gfx/color.h"

class Debris
{
 public:
  explicit Debris(const glm::vec3 &center);
  ~Debris();
  void draw() const;
  void update(unsigned int etime);

 private:
  Debris(const Debris& debris) = delete;
  Debris& operator=(const Debris& debris) = delete;
  std::vector<Cube> cubes_;
};

#endif /* !DEBRIS_H_ */
