#ifndef CAMERA_H
#define CAMERA_H

#include <cubefield/cubefield.h>

#define ROLL_COEF 200.0

class World;

enum camView{FOLLOW, UP};

class Camera
{
 public:
  Camera(const glm::vec3 &pos, const glm::vec3 &target);

  void set_pos(const glm::vec3 &pos) { pos_ = pos; }
  const glm::vec3& pos() const { return pos_; }
  void set_roll(const float roll) { roll_ = roll; }
  float roll() const { return roll_; }
  void set_x(const float x) { pos_.x = x; }
  void set_y(const float y) { pos_.y = y; }
  void set_z(const float z) { pos_.z = z; }
  float x() const { return pos_.x; }
  float y() const { return pos_.y; }
  float z() const { return pos_.z; }
  void set_target(const glm::vec3 &target) { target_ = target; }
  const glm::vec3& target() const { return target_; }

  void update(unsigned int etime, World *world);
  void changeView(camView view);

 private:
  glm::vec3   pos_;
  glm::vec3   target_;
  float   roll_;
  camView view_;
};

#endif
