#include "./debris.h"
#include <cstdlib>
#include "gfx/draw.h"


Debris::Debris(const glm::vec3 &center)
  : cubes_(std::vector<Cube>())
{
  gfx::Color c(1, 1, 1);
  for (int i = 0; i < 100; i++)
  {
      cubes_.push_back(Cube(center));
      cubes_.back().set_color(c);
      cubes_.back().speed = glm::vec3((rand() % 20-10) / 120.0,
                                      (rand() % 10) / 120.0,
                                      (rand() % 20-10) / 120.0);
    }
}

Debris::~Debris()
{
}

void Debris::draw() const
{
  for (auto it = cubes_.begin(); it != cubes_.end(); it++)
  {
    gfx::draw_cube(*it, 0.5);
  }
}

void Debris::update(unsigned int etime)
{
  for (auto it = cubes_.begin(); it != cubes_.end(); it++)
  {
    it->center += it->speed * (static_cast<float>(etime)/2.0f);
    it->speed.x *= 0.85;
    it->speed.y *= 0.85;
    it->speed.z *= 0.85;
  }
}
