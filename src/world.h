#ifndef WORLD_H
#define WORLD_H

#include <memory>

#include "cubefield/cubefield.h"
#include "./ship.h"

class World
{
  public:
    World();
    virtual ~World();
    void draw() const;
    void update(unsigned int etime, unsigned int time);

    Ship* ship() { return ship_.get(); }
    Cubefield* cubefield() { return cubefield_.get(); }
    unsigned int time;

  private:
    World& operator=(const World& world) = delete;
    World(const World& world) = delete;

    std::unique_ptr<Ship> ship_;
    std::unique_ptr<Cubefield> cubefield_;
};

#endif
