#include "./world_object.h"

WorldObject::WorldObject()
  : position(glm::vec3(0.0f)),
    scale(glm::vec3(0.0f)),
    rotation(glm::vec3(0.0f)),
    speed(glm::vec3(0.0f)),
    acceleration(0.0f),
    friction(0.0f)
{
}

WorldObject::~WorldObject()
{
}
