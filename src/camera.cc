#include "./camera.h"
#include <cmath>
#include "gfx/view.h"
#include "./world.h"

Camera::Camera(const glm::vec3 &pos, const glm::vec3 &target)
  : pos_(pos), target_(target), roll_(0), view_(FOLLOW)
{}

void Camera::changeView(camView view)
{
  view_ = view;
}

void Camera::update(unsigned int /*etime*/, World *world)
{
  switch (view_)
  {
    case FOLLOW:
      // Always look at the ship
      set_target(world->ship()->position);
      // Position the camera juste above and behind the ship
      set_pos(world->ship()->position);
      set_y(y() + 5.0);
      set_x(x() - 7.0);
      set_roll(world->ship()->speed.z * ROLL_COEF);
      break;

    case UP:
      set_target(world->ship()->position);
      set_pos(world->ship()->position);
      set_y(y() + 50.0);
      set_x(x() - 1.0);
      set_roll(0);
      break;
  }


  // Update camera position
  gfx::update_view(*this);
}
