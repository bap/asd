#include "./world.h"
#include "gfx/draw.h"

World::World()
  : time(0),
    ship_(std::unique_ptr<Ship> (new Ship)),
    cubefield_(std::unique_ptr<Cubefield> (new Cubefield))
{}

World::~World()
{
}

void World::update(unsigned int etime, unsigned int time_)
{
  if (ship_)
    time = time_;
  ship_->addSpeed(etime/1000.0*ship_->acceleration/10.0, 0, 0);
  ship_->update(etime, this);
  cubefield_->update(etime, this);
}

void World::draw() const
{
  cubefield_->draw();
  ship_->draw();
  gfx::draw_score(ship_->score/100, ship_->highscore/100);
}
