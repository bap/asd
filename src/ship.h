#ifndef SHIP_H_
# define SHIP_H_

#include <memory>

#include "cubefield/cube.h"
#include "./debris.h"
#include "./world_object.h"
#include "gfx/color.h"

#define MAX_SHIP_SPEED          0.2

class World;

class Ship : public WorldObject
{
 public:
  Ship();
  ~Ship();
  void draw() const;
  void update(unsigned int etime, World *world);
  void addSpeed(float x, float y, float z);
  void setColor(int r, int g, int b);
  void explode();
  void repair();

  bool god_mode;
  unsigned int score;
  unsigned int highscore;

 private:
  Ship(const Ship& ship) = delete;
  Ship& operator=(const Ship& ship) = delete;

  unsigned int old_score;
  std::unique_ptr<Cube> mesh_;
  gfx::Color color_;
  bool broken_;
  std::unique_ptr<Debris> debris_;
};

#endif /* !SHIP_H_ */
