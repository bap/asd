#include <event/event_manager.h>

#include <utility>

namespace event
{
  EventManager::EventManager()
    : listener_map_(std::unordered_multimap<std::string, IEventListener&>()),
      event_queue_(std::queue<std::unique_ptr<Event>>())
  {
  }

  EventManager::~EventManager()
  {
  }

  EventManager& EventManager::get_instance()
  {
      static EventManager* instance = nullptr;

      if (!instance)
          instance = new EventManager;
      return *instance;
  }

  void EventManager::register_listener(IEventListener    *listener,
                                       const std::string& event_type)
  {
    listener_map_.insert(std::pair<std::string, IEventListener&>(event_type,
                                                                 *listener));
  }

  void EventManager::unregister_listener(const IEventListener& listener,
                                         const std::string&    event_type)
  {
    map_iterator it = listener_map_.find(event_type);

    for (; it != listener_map_.end() && it->first == event_type; ++it)
    {
      if (&(it->second) == &listener)
      {
        listener_map_.erase(it);
        break;
      }
    }
  }

  void EventManager::push_event(Event* event)
  {
    event_queue_.push(std::unique_ptr<Event>(event));
  }

  void EventManager::fetch_event(const Event& event)
  {
    std::pair<const_map_iterator, const_map_iterator> it =
      listener_map_.equal_range(event.get_type());

    for (const_map_iterator elts_it = it.first; elts_it != it.second; ++elts_it)
    {
      elts_it->second.notify(event);
    }
  }

  void EventManager::fetch_events()
  {
    while (!event_queue_.empty())
    {
      fetch_event(*(event_queue_.front().get()));
      event_queue_.pop();
    }
  }
}  // namespace event
