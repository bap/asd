#ifndef EVENT_MANAGER_H_
# define EVENT_MANAGER_H_

# include <unordered_map>
# include <string>
# include <queue>
# include <memory>

# include "event/events/event.h"
# include "event/event_listener.h"

namespace event
{
  class EventManager
  {
    public:
      ~EventManager();

      static EventManager& get_instance();

      typedef std::unordered_multimap<std::string, IEventListener&>::const_iterator const_map_iterator;
      typedef std::unordered_multimap<std::string, IEventListener&>::iterator map_iterator;

      // Associate a listener to an event
      void register_listener(IEventListener *listener,
                             const std::string& event_type);
      // Do not associate anymore the given listener to an event
      void unregister_listener(const IEventListener& listener,
                               const std::string& event_type);
      // Push an event to be processed later
      void push_event(Event* event);
      // Process an event immediatly (without pushing it to the queue)
      void fetch_event(const Event& event);
      // Process all events in the queue
      void fetch_events();

    private:
      EventManager();
      EventManager(const EventManager&) = delete;
      EventManager& operator=(const EventManager&) = delete;

      std::unordered_multimap<std::string, IEventListener&> listener_map_;
      std::queue<std::unique_ptr<Event>> event_queue_;
  };
}  // namespace event

#endif /* !EVENT_MANAGER_H_ */
