#ifndef EVENT_LISTENER_H_
# define EVENT_LISTENER_H_

namespace event
{
  class Event;

  class IEventListener
  {
    public:
      virtual ~IEventListener() {}
      virtual void notify(const Event& event) = 0;
  };
}  // namespace event

#endif /* !EVENT_LISTENER_H_ */
