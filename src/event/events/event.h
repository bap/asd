#ifndef EVENT_H_
# define EVENT_H_

# include <string>
# include "event/event_listener.h"

class Cubefield;

namespace event
{
  class IEventListener;

  class Event
  {
    public:
      explicit Event(const std::string& type);
      virtual ~Event();

      void accept(IEventListener *listener) { listener->notify(*this); }
      const std::string& get_type() const { return type_; }

    private:
      const std::string type_;
  };
}  // namespace event

#endif /* !EVENT_H_ */
