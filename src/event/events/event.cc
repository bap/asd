#include <event/events/event.h>

namespace event
{
  Event::Event(const std::string& type)
    : type_(type)
  {
  }

  Event::~Event()
  {
  }
}  // namespace event
